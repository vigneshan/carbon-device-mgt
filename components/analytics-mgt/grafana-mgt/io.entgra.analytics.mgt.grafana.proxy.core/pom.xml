<?xml version="1.0" encoding="UTF-8"?>
<!--
~ Copyright (c) 2021, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
~
~ Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
~ Version 2.0 (the "License"); you may not use this file except
~ in compliance with the License.
~ You may obtain a copy of the License at
~
~    http://www.apache.org/licenses/LICENSE-2.0
~
~ Unless required by applicable law or agreed to in writing,
~ software distributed under the License is distributed on an
~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~ KIND, either express or implied. See the License for the
~ specific language governing permissions and limitations
~ under the License.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <parent>
        <groupId>org.wso2.carbon.devicemgt</groupId>
        <artifactId>grafana-mgt</artifactId>
        <version>5.0.10-SNAPSHOT</version>
        <relativePath>../pom.xml</relativePath>
    </parent>

    <modelVersion>4.0.0</modelVersion>
    <artifactId>io.entgra.analytics.mgt.grafana.proxy.core</artifactId>
    <packaging>bundle</packaging>
    <name>Entgra - Grafana API Handler Core</name>
    <description>Entgra - Grafana API Handler Core</description>
    <url>http://entgra.io</url>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.felix</groupId>
                <artifactId>maven-scr-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <configuration>
                    <destFile>${basedir}/target/coverage-reports/jacoco-unit.exec</destFile>
                </configuration>
                <executions>
                    <execution>
                        <id>jacoco-initialize</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>jacoco-site</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                        <configuration>
                            <dataFile>${basedir}/target/coverage-reports/jacoco-unit.exec</dataFile>
                            <outputDirectory>${basedir}/target/coverage-reports/site</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.felix</groupId>
                <artifactId>maven-bundle-plugin</artifactId>
                <extensions>true</extensions>
                <configuration>
                    <instructions>
                        <Bundle-SymbolicName>${project.artifactId}</Bundle-SymbolicName>
                        <Bundle-Name>${project.artifactId}</Bundle-Name>
                        <Bundle-Version>${carbon.device.mgt.version}</Bundle-Version>
                        <Bundle-Description>Grafana API Management Core Bundle</Bundle-Description>
                        <Private-Package>io.entgra.analytics.mgt.grafana.proxy.core.internal</Private-Package>
                        <Import-Package>
                            io.entgra.analytics.mgt.grafana.proxy.common.*,
                            javax.xml.parsers;version="${javax.xml.parsers.import.pkg.version}";resolution:=optional,
                            javax.xml.bind.annotation,
                            javax.xml.bind,
                            org.apache.commons.lang,
                            org.wso2.carbon,
                            org.wso2.carbon.device.mgt.common.*,
                            org.wso2.carbon.device.mgt.core.*
                            io.entgra.application.mgt.core.*
                        </Import-Package>
                        <Export-Package>
                            !org.wso2.carbon.email.sender.core.internal,
                            io.entgra.analytics.mgt.grafana.proxy.core.*
                        </Export-Package>
                        <Embed-Dependency>
                            scribe;scope=compile|runtime;inline=false,
                        </Embed-Dependency>
                        <DynamicImport-Package>*</DynamicImport-Package>
                    </instructions>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>org.eclipse.osgi</groupId>
            <artifactId>org.eclipse.osgi</artifactId>
        </dependency>
        <dependency>
            <groupId>org.eclipse.osgi</groupId>
            <artifactId>org.eclipse.osgi.services</artifactId>
        </dependency>
		<dependency>
			<groupId>org.wso2.orbit.org.scannotation</groupId>
			<artifactId>scannotation</artifactId>
		</dependency>
        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>org.wso2.carbon.device.mgt.common</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.utils</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.logging</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.core</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>io.entgra.analytics.mgt.grafana.proxy.common</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>org.wso2.carbon.device.mgt.core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>io.entgra.application.mgt.core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents.wso2</groupId>
            <artifactId>httpclient</artifactId>
            <version>4.1.1.wso2v1</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>org.wso2.carbon.identity.jwt.client.extension</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>javax.ws.rs-api</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>jsr311-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.javassist</groupId>
            <artifactId>javassist</artifactId>
        </dependency>
        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-api-mockito</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.ndatasource.core</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>commons-lang</groupId>
                    <artifactId>commons-lang</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>commons-codec.wso2</groupId>
            <artifactId>commons-codec</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.h2database.wso2</groupId>
            <artifactId>h2-database-engine</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
        </dependency>
        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-module-testng</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.user.core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.user.api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.registry.api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.registry.core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.tomcat.wso2</groupId>
            <artifactId>jdbc-pool</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.base</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.governance</groupId>
            <artifactId>org.wso2.carbon.governance.api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.axis2.transport</groupId>
            <artifactId>axis2-transport-mail</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.ws.commons.axiom.wso2</groupId>
            <artifactId>axiom</artifactId>
        </dependency>
        <!--dependency>
            <groupId>org.apache.ws.commons.axiom.wso2</groupId>
            <artifactId>axiom-impl</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.ws.commons.axiom.wso2</groupId>
            <artifactId>axiom</artifactId>
        </dependency-->
        <dependency>
            <groupId>org.apache.axis2.wso2</groupId>
            <artifactId>axis2</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.identity.inbound.auth.oauth2</groupId>
            <artifactId>org.wso2.carbon.identity.oauth.stub</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.tomcat</groupId>
            <artifactId>tomcat</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.tomcat</groupId>
            <artifactId>tomcat-servlet-api</artifactId>
        </dependency>

        <!--Ntask dependencies-->
        <dependency>
            <groupId>org.wso2.carbon.commons</groupId>
            <artifactId>org.wso2.carbon.ntask.core</artifactId>
        </dependency>


        <dependency>
            <groupId>commons-collections.wso2</groupId>
            <artifactId>commons-collections</artifactId>
        </dependency>

        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>org.wso2.carbon.email.sender.core</artifactId>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
        </dependency>

        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-annotations</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>org.wso2.carbon.apimgt.annotations</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.event-processing</groupId>
            <artifactId>org.wso2.carbon.event.processor.stub</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.multitenancy</groupId>
            <artifactId>org.wso2.carbon.tenant.mgt</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-validator</groupId>
            <artifactId>commons-validator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>io.entgra.server.bootup.heartbeat.beacon</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>
    </dependencies>



</project>
